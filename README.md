<!-- 
    SPDX-FileCopyrightText: 2011-2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: AGPL-3.0-or-later 
-->
<!-- 
    SPDX-FileCopyrightText: (c) 2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: AGPL-3.0-or-later 
-->

# savapage-nfc-reader

This module implements a Network NFC Reader Service that makes a CCID compatible reader
communicate with SavaPage server.

### License

This module is part of the SavaPage project <https://www.savapage.org>,
copyright (c) 2020 Datraverse B.V. and licensed under the
[GNU Affero General Public License (AGPL)](https://www.gnu.org/licenses/agpl.html)
version 3, or (at your option) any later version.

[<img src="./img/reuse-horizontal.png" title="REUSE Compliant" alt="REUSE Software" height="25"/>](https://reuse.software/)

### Join Efforts, Join our Community

SavaPage Software is produced by Community Partners and consumed by Community Members. If you want to modify and/or distribute our source code, please join us as Development Partner. By joining the [SavaPage Community](https://wiki.savapage.org) you can help build a truly Libre Print Management Solution. Please contact [info@savapage.org](mailto:info@savapage.org).

### Issue Management

[https://issues.savapage.org](https://issues.savapage.org)

### Dependencies

#### Embedded modules

[Inih](https://github.com/benhoyt/inih) - simple .INI file parser. 

#### Build

Make sure to build [https://gitlab.com/savapage/xmlrpcpp]() in the sibling directory `../xmlrpcpp` first.

The PC/SC Lite development package is needed to build the binary. Install with this command:

    $ sudo apt-get install libpcsclite-dev

#### Runtime

    $ sudo apt-get install libccid pcscd

If using an ACS Reader additionally install:
 
    $ sudo apt-get install libacsccid1

### Problem shooting 

#### NFC Reader not connected

When `savapage-nfc-reader` does not connect to the NFC Reader this is probably caused by the kernel automatically loading the pn533 driver. With the pn533 driver enabled, `pcscd` encounters a "Can't claim interface" error as reported in the `/var/log/syslog` file.

_Therefore the pn533 and nfc driver should be disabled in the kernel._

Edit the `/etc/modprobe.d/blacklist.conf` file like this:
     
    $ sudo vi /etc/modprobe.d/blacklist.conf
    
.. and make sure that the file contains the following 2 lines:
    
    install nfc /bin/false
    install pn533 /bin/false

#### Card swipe not detected

Install the PC/SC diagnostic tools ...

    $ sudo apt-get install pcsc-tools

.. and run:
 
    $ pcsc_scan
    
This utility scans available smart card readers and print card insertion and removal events. When swiping a card you should see "Card inserted" and "Card removed" printed on stdout. 

Please contact [support@savapage.org](mailto:support@savapage.org). when `pcsc_scan` prints card events, but `savapage-nfc-reader` does not.
    

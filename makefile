#
# This file is part of the SavaPage project <https://www.savapage.org>.
# Copyright (c) 2020 Datraverse B.V.
# Author: Rijk Ravestein.
#
# SPDX-FileCopyrightText: © 2020 Datraverse B.V. <info@datraverse.com>
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For more information, please contact Datraverse B.V. at this
# address: info@datraverse.com
#

#-------------------------------------------------------------------------
# Prerequisite for make:
# 	https://gitlab.com/savapage/xmlrpcpp
#-------------------------------------------------------------------------
# Prerequisite for make:
# 	$ sudo apt-get install libpcsclite-dev pkg-config
#-------------------------------------------------------------------------
# Runtime prerequisite on Debian/Raspbian:
# 	$ sudo apt-get install libccid libacsccid1 pcscd
#-------------------------------------------------------------------------

#-------------------------------------------------------------------------
# 'make' command line parameters:
# 	PRODUCT_VERSION=x.x.x
#-------------------------------------------------------------------------

PROGRAM:=savapage-nfc-reader

INI_READER_CPP=INIReader
INI_READER_C=ini

PCSC_LIB := $(shell pkg-config --libs libpcsclite)
PCSC_CFLAGS := $(shell pkg-config --cflags libpcsclite)

LIBS := $(PCSC_LIB) ../xmlrpcpp/libXmlRpc.a -lpthread

CFLAGS += $(PCSC_CFLAGS) -I../xmlrpcpp/src

ifdef PRODUCT_VERSION 
CFLAGS += -DPRODUCT_VERSION=\"$(PRODUCT_VERSION)\"
endif

TRG := ./target

.PHONY: all
all: init $(TRG)/$(PROGRAM)
 
.PHONY: init
init:
	@mkdir -p $(TRG)  
 
$(TRG)/$(PROGRAM) : $(TRG)/$(PROGRAM).o $(TRG)/$(INI_READER_CPP).o $(TRG)/$(INI_READER_C).o
	g++ $(CFLAGS) $(LFLAGS) -o $@ $< $(LIBS) $(TRG)/$(INI_READER_CPP).o $(TRG)/$(INI_READER_C).o 
	strip $@

$(TRG)/$(PROGRAM).o: $(PROGRAM).cc
	g++ -c $(CFLAGS) $(LFLAGS) -o $@ $<

$(TRG)/$(INI_READER_CPP).o: $(INI_READER_CPP).cpp
	g++ -c -o $@ $<

$(TRG)/$(INI_READER_C).o: $(INI_READER_C).c
	g++ -c -o $@ $<

.PHONY: clean
clean:
	@rm -f $(TRG)/$(PROGRAM) $(TRG)/*.o

# end-of-file

/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2011-2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: 2011-2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */

/*
 * Credits:
 * http://support.gemalto.com/fileadmin/user_upload/drivers/ToolsAPISamples/ReaderTrack.zip
 * for sample Visual Studio Project
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <iostream>

#include <unistd.h> // exec(), ...
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/syslog.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <pthread.h>
#include <signal.h>

#include <PCSC/wintypes.h> /* help Eclipse to check the code  */
#include <PCSC/pcsclite.h>
#include <PCSC/winscard.h>

#include <XmlRpc.h>

#include "INIReader.h"

//-------------------------------------------------------------
// MACROS
//-------------------------------------------------------------
#define CARD_STATUS_CHANGE_TIMEOUT_MSEC 2000

#ifndef MAXIMUM_SMARTCARD_READERS
#define MAXIMUM_SMARTCARD_READERS 5
#endif

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif

/* PCSC error message pretty print macro + request shutdown */
#define CHECK_PCSC_RV(rv, text) \
if (rv != SCARD_S_SUCCESS) \
{ \
	printf(text ": %s (0x%lX)\n", pcsc_stringify_error(rv), rv); \
	syslog(LOG_ERR, text ": %s (0x%lX)", pcsc_stringify_error(rv), rv); \
	glShutdownRequested = true; \
	goto end; \
} \
else \
{ \
	/* printf(text ": OK\n\n"); */ \
}

/* PCSC error message pretty print macro + request shutdown */
#define PCSC_ERROR(text) \
printf(text"\n"); \
syslog(LOG_ERR, text); \
glShutdownRequested = true; \
goto end;

/**
 * See XmlRpcServlet.properties in SavaPage Server Java project.
 */
#define XMLRPC_METHOD_PFX "rfid-event."

//-------------------------------------------------------------
// CONSTANTS
//-------------------------------------------------------------
static std::string glProgramVersion("1.2.0");

static const std::string API_ID = "savapage-nfc-reader";
static const std::string API_KEY =
		"302c021447ca02bbfe3234e7d993920c8b2741609b6550be0214116c5704efe9ed019a1ac6374c0a074cae6965c2";

static const long SELECT_TIMEOUT_SECS = 3L;

static const int SERVER_RC_ACCEPT = 0;
static const int SERVER_RC_DENY = 1;
static const int SERVER_RC_EXCEPTION = 99;
static const int SERVER_RC_DISCONNECT = -1;

//------------------------------------------------------------
// GLOBALS
//------------------------------------------------------------
const char* glProgramName;

static BOOL glVerbose = FALSE;
static BOOL glShutdownRequested = FALSE;

/*
 * Global variables
 */
static struct {

	std::string host;
	short port;
	short reader_port;
	int connectTimeout; // milliseconds

	bool beep;
	bool aplay;
	std::string aplayCardSwipe;
	std::string aplayServerAccept;
	std::string aplayServerDeny;
	std::string aplayServerException;
	std::string aplayServerDisconnect;

	std::string runtime;

	std::string scriptStartService;
	std::string scriptCardSwipe;
	std::string scriptServerAccept;
	std::string scriptServerDeny;
	std::string scriptServerException;
	std::string scriptServerDisconnect;

} conf;

//-------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------

/**
 *
 */
void signalHandler(int sig) {

	const char* msg;

	const char* msgSigTerm = "Terminated, shutting down ...";
	const char* msgSigInt = "Interrupted, shutting down ...";

	if (sig == SIGTERM) {
		msg = msgSigTerm;
	} else {
		msg = msgSigInt;
	}

	printf("\n%s\n", msg);
	syslog(LOG_NOTICE, "%s", msg);

	glShutdownRequested = TRUE;
}

/**
 *
 */
char* get_time(char * buff, size_t buffSize) {
	time_t rawtime;
	struct tm * timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buff, buffSize, "%H:%M:%S", timeinfo);
	return buff;
}

/**
 * Gives a beep through the PC speaker.
 */
void beep(const char* freq, const char* duration) {
	pid_t child_pid;
	int status;

	/* Duplicate this process. */
	child_pid = fork();
	if (child_pid != 0) {
		/*
		 * This is the parent process: wait for child process to finish to
		 * prevent zombie process.
		 */
		waitpid(child_pid, &status, 0);
		return;
	} else {
		/* Now execute PROGRAM, searching for it in the path. */
		execlp("beep", "beep", "-f", freq, "-l", duration, NULL);
		/* The execlp function returns only if an error occurs. */
		fprintf(stderr, "error in execlp (beep)\n");
		syslog(LOG_ERR, "error in execlp (beep)");
		exit(0);
	}
}

/**
 * Plays a sound file (using audio card) or a script.
 */
void execAudioOrScript(const std::string & soundFile,
		const std::string & script) {

	if (soundFile.length() == 0 && script.length() == 0) {
		return;
	}

	pid_t child_pid;
	int status;

	/* Duplicate this process. */
	child_pid = fork();

	if (child_pid != 0) {
		/*
		 * This is the parent process: wait for child process to finish to
		 * prevent zombie process.
		 */
		waitpid(child_pid, &status, 0);
		return;

	} else {

		if (soundFile.length() > 0) {
			syslog(LOG_DEBUG, "aplay --quiet %s", soundFile.c_str());
			/* This will work on the Raspberry Pi. */
			execlp("aplay", "aplay", "--quiet", soundFile.c_str(), NULL);
			/* The execlp function returns only if an error occurs. */
			fprintf(stderr, "error in execlp (aplay)\n");
			syslog(LOG_ERR, "error in execlp (aplay)");
		} else {
			syslog(LOG_DEBUG, "%s %s", conf.runtime.c_str(), script.c_str());
			execlp(conf.runtime.c_str(), conf.runtime.c_str(), script.c_str(),
			NULL);
			/* The execlp function returns only if an error occurs. */
			fprintf(stderr, "error in script %s\n", script.c_str());
			syslog(LOG_ERR, "error in script %s", script.c_str());
		}
		exit(0);
	}
}

/**
 * Notifies an event to the SavaPage server, using XML-RPC.
 */
int notifyEvent(const char *method, XmlRpc::XmlRpcValue &args) {

	int rc = SERVER_RC_DISCONNECT;

	using namespace XmlRpc;

	std::string message;

	try {
		// Create a client and connect to the server at hostname:port
		XmlRpcClient client(static_cast<const char*>(conf.host.c_str()),
				conf.port, "/xmlrpc");

		XmlRpcValue result;

		if (client.execute(method, args, result,
				(double) conf.connectTimeout / 1000)) {

			if (client.isFault()) {
				message = "fault response";
			} else {
				rc = static_cast<int>(result["rc"]);
				message = static_cast<std::string>(result["message"]);
			}
		} else {
			message = "no connection or timeout";
		}

		// Mantis #488.
		client.close();

	} catch (XmlRpcException & ex) {
		message = ex.getMessage();
	} catch (...) {
		message = "unknown exception";
	}

	if (message.length() > 0) {

		int syslogPri;
		FILE * stream;

		if (SERVER_RC_DISCONNECT == rc) {
			syslogPri = LOG_WARNING;
		} else if (SERVER_RC_EXCEPTION == rc) {
			syslogPri = LOG_ERR;
		} else {
			syslogPri = LOG_INFO;
		}

		if (LOG_ERR == syslogPri) {
			stream = stderr;
		} else {
			stream = stdout;
		}

		fprintf(stream, "%s:%i reports: %s\n", conf.host.c_str(), conf.port,
				message.c_str());

		syslog(syslogPri, "%s:%i reports: %s", conf.host.c_str(), conf.port,
				message.c_str());
	}

	return rc;
}

/**
 * Writes UID of Chipcard to stdout and notifies the host.
 *
 * Adapted from nfc-utils.c: print_hex().
 */
void notifyCardUid(const BYTE * pbtData, const size_t szBytes,
		const char* szReader) {

	size_t szPos;

	static const char* freqSuccess = "500";
	static const char* durationSuccess = "500";

	static const char* freqFail = "150";
	static const char* durationFail = "700";

	/*
	 * Notify the card swipe.
	 */
	if (conf.beep) {
		beep(freqSuccess, durationSuccess);
	}

	execAudioOrScript(conf.aplayCardSwipe, conf.scriptCardSwipe);

	printf("UID: ");

	std::string rfid;

	for (szPos = 0; szPos < szBytes; szPos++) {
		char hex[8];
		sprintf(hex, "%02x", pbtData[szPos]); // lower case hex !!
		rfid += hex;
	}

	std::cout << rfid << " [" << szReader << "]" << std::endl;

	syslog(LOG_INFO, "UID: %s [%s]", rfid.c_str(), szReader);

	XmlRpc::XmlRpcValue args;

	args[0] = API_ID;
	args[1] = API_KEY;
	args[2] = rfid;

	const int rc = notifyEvent(XMLRPC_METHOD_PFX"cardSwipe", args);

	if (rc == SERVER_RC_ACCEPT) {
		if (conf.beep) {
			beep(freqSuccess, durationSuccess);
		}
		execAudioOrScript(conf.aplayServerAccept, conf.scriptServerAccept);

	} else if (rc == SERVER_RC_DENY) {
		if (conf.beep) {
			beep(freqFail, durationFail);
		}
		execAudioOrScript(conf.aplayServerDeny, conf.scriptServerDeny);

	} else if (rc == SERVER_RC_EXCEPTION) {
		if (conf.beep) {
			beep(freqFail, durationFail);
			sleep(1);
			beep(freqFail, durationFail);
		}
		execAudioOrScript(conf.aplayServerException,
				conf.scriptServerException);

	} else {
		if (conf.beep) {
			beep(freqFail, durationFail);
			sleep(1);
			beep(freqFail, durationFail);
			sleep(1);
			beep(freqFail, durationFail);
		}
		execAudioOrScript(conf.aplayServerDisconnect,
				conf.scriptServerDisconnect);

	}

}

/**
 * Parses the program name from the command-line invocation.
 */
const char* parseProgramName(const char* programPath) {
	const char * name;
	for (name = programPath + strlen(programPath);
			name != programPath && *name != '/'; name--)
		;
	if (*name == '/') {
		name++;
	}
	return name;
}

#define CLI_OPT_HOST "--host"
#define CLI_OPT_PORT "--port"
#define CLI_OPT_READER_PORT "--reader-port"
#define CLI_OPT_HELP "--help"
#define CLI_OPT_HELP_SHORT "-h"
#define CLI_OPT_TIMEOUT "--timeout"
#define CLI_OPT_VERBOSE "--verbose"
#define CLI_OPT_VERBOSE_SHORT "-v"

/**
 * Writes program usage to stdout.
 */
void printUsage() {
	printf(
			"____________________________________________________________________________\n"
					"SavaPage Network Card Reader v%s\n"
					"Compiled with PC/SC Lite v%s for ACS USB NFC Readers.\n"
					"(c) 2011-2020, Datraverse B.V.\n"
					"\n"
					"License: GNU AGPL version 3 or later <https://www.gnu.org/licenses/agpl.html>.\n"
					"         This is free software: you are free to change and redistribute it.\n"
					"         There is NO WARRANTY, to the extent permitted by law.\n"
					"\n"
					"Usage: %s [options]\n"
			CLI_OPT_HOST"         IP address of SavaPage server.\n"
			CLI_OPT_PORT"         IP port of SavaPage server (default: 8631).\n"
			CLI_OPT_TIMEOUT"      Connect timeout (milliseconds) to server (default: 5000).\n"
			CLI_OPT_READER_PORT"  IP port of this reader (default: 7772).\n"
			CLI_OPT_VERBOSE_SHORT "," CLI_OPT_VERBOSE"   Extra output (for debug purposes only).\n"
			CLI_OPT_HELP_SHORT "," CLI_OPT_HELP "      Shows this help text.\n"
			"\n"
			"\n", glProgramVersion.c_str(), PCSCLITE_VERSION_NUMBER,
			glProgramName);
}

/**
 *
 */
void setConfAttrFilePath(const char *cwd, std::string & confAttr,
		const std::string & file) {

	if (file.length() > 0) {
		confAttr = cwd;
		confAttr += "/";
		confAttr += file;
	}
}

/**
 * Handles the command-line parameters.
 */
BOOL handleParms(int argc, const char *argv[]) {

	conf.port = 8631;
	conf.connectTimeout = 5000;
	conf.reader_port = 7772;
	conf.beep = false;
	conf.aplay = false;
	conf.aplayCardSwipe = "";
	conf.aplayServerAccept = "";
	conf.aplayServerDeny = "";
	conf.aplayServerException = "";

	conf.runtime = "";
	conf.scriptStartService = "";
	conf.scriptCardSwipe = "";
	conf.scriptServerAccept = "";
	conf.scriptServerDeny = "";
	conf.scriptServerException = "";

	glProgramName = parseProgramName(argv[0]);

	static const std::string INI_SECTION_CONNECTIVITY = "connectivity";
	static const std::string INI_SECTION_SOUND = "sound";
	static const std::string INI_SECTION_SCRIPT = "script";

	/*
	 * Read the .ini file from current directory.
	 */
	char cwd[1024];

	if (getcwd(cwd, sizeof(cwd)) != NULL) {

		std::string cfgFilename = cwd;
		cfgFilename += "/";
		cfgFilename += glProgramName;
		cfgFilename += ".ini";

		INIReader reader(cfgFilename);

		if (reader.ParseError() >= 0) {

			syslog(LOG_INFO, "Reading configuration file: %s",
					cfgFilename.c_str());

			conf.host = reader.Get(INI_SECTION_CONNECTIVITY, "host", "");
			conf.port = reader.GetInteger(INI_SECTION_CONNECTIVITY, "port",
					conf.port);
			conf.connectTimeout = reader.GetInteger(INI_SECTION_CONNECTIVITY,
					"timeout", conf.connectTimeout);
			conf.reader_port = reader.GetInteger(INI_SECTION_CONNECTIVITY,
					"reader-port", conf.reader_port);

			std::string player = reader.Get(INI_SECTION_SOUND, "player", "");

			conf.beep = (player == "beep");
			conf.aplay = (player == "aplay");

			if (conf.aplay) {

				setConfAttrFilePath(cwd, conf.aplayCardSwipe,
						reader.Get(INI_SECTION_SOUND, "wav-card-swipe", ""));

				setConfAttrFilePath(cwd, conf.aplayServerAccept,
						reader.Get(INI_SECTION_SOUND, "wav-server-accept", ""));

				setConfAttrFilePath(cwd, conf.aplayServerDeny,
						reader.Get(INI_SECTION_SOUND, "wav-server-deny", ""));

				setConfAttrFilePath(cwd, conf.aplayServerDisconnect,
						reader.Get(INI_SECTION_SOUND, "wav-server-disconnect",
								""));

				setConfAttrFilePath(cwd, conf.aplayServerException,
						reader.Get(INI_SECTION_SOUND, "wav-server-exception",
								""));
			}

			conf.runtime = reader.Get(INI_SECTION_SCRIPT, "runtime", "");

			if (conf.runtime.length() > 0) {

				setConfAttrFilePath(cwd, conf.scriptStartService,
						reader.Get(INI_SECTION_SCRIPT, "script-start-service",
								""));

				setConfAttrFilePath(cwd, conf.scriptCardSwipe,
						reader.Get(INI_SECTION_SCRIPT, "script-card-swipe",
								""));

				setConfAttrFilePath(cwd, conf.scriptServerAccept,
						reader.Get(INI_SECTION_SCRIPT, "script-server-accept",
								""));

				setConfAttrFilePath(cwd, conf.scriptServerDeny,
						reader.Get(INI_SECTION_SCRIPT, "script-server-deny",
								""));

				setConfAttrFilePath(cwd, conf.scriptServerDisconnect,
						reader.Get(INI_SECTION_SCRIPT,
								"script-server-disconnect", ""));

				setConfAttrFilePath(cwd, conf.scriptServerException,
						reader.Get(INI_SECTION_SCRIPT,
								"script-server-exception", ""));
			}

		} else {
			syslog(LOG_ERR, "Error reading configuration file: %s",
					cfgFilename.c_str());
		}

	} else {
		perror("getcwd() error");
		return FALSE;
	}

	//-----------------------------------------
	for (int i = 1; i < argc; i++) {

		if (!strcmp(argv[i], CLI_OPT_HELP_SHORT)
				|| !strcmp(argv[i], CLI_OPT_HELP)) {
			printUsage();
			exit(EXIT_SUCCESS);
		} else if (!strcmp(argv[i], CLI_OPT_HOST)) {
			if (++i < argc) {
				conf.host = argv[i];
			} else {
				printUsage();
				exit(EXIT_FAILURE);
			}
		} else if (!strcmp(argv[i], CLI_OPT_PORT)) {
			if (++i < argc) {
				conf.port = atoi(argv[i]);
			} else {
				printUsage();
				exit(EXIT_FAILURE);
			}
		} else if (!strcmp(argv[i], CLI_OPT_TIMEOUT)) {
			if (++i < argc) {
				conf.connectTimeout = atoi(argv[i]);
			} else {
				printUsage();
				exit(EXIT_FAILURE);
			}
		} else if (!strcmp(argv[i], CLI_OPT_READER_PORT)) {
			if (++i < argc) {
				conf.reader_port = atoi(argv[i]);
			} else {
				printUsage();
				exit(EXIT_FAILURE);
			}
		} else if (!strcmp(argv[i], CLI_OPT_VERBOSE_SHORT)
				|| !strcmp(argv[i], CLI_OPT_VERBOSE)) {
			glVerbose = TRUE;
		} else {
			printUsage();
			exit(EXIT_SUCCESS);
		}
	}

	if (conf.host.length() == 0) {
		printUsage();
		exit(EXIT_FAILURE);
	}

	return TRUE;
}

/**
 * Prints a message with errno and exits with EXIT_FAILURE.
 */
void error(const char *msg) {
	perror(msg);
	exit(EXIT_FAILURE);
}

/**
 *
 */
void *server_thread(void * arg) {

	short portno = conf.reader_port;
	socklen_t clilen;

	struct sockaddr_in serv_addr, cli_addr;
	int n;

	/*
	 * Create a socket.
	 */
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		error("ERROR opening socket");
	}

	const int optVal = 1;
	const socklen_t optLen = sizeof(optVal);

	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (void*) &optVal, optLen)) {
		error("ERROR on setsockopt");
	}

	// clear address structure
	bzero((char *) &serv_addr, sizeof(serv_addr));

	/* setup the host_addr structure for use in bind call */
	// server byte order
	serv_addr.sin_family = AF_INET;

	// automatically be filled with current host's IP address
	serv_addr.sin_addr.s_addr = INADDR_ANY;

	// convert short integer value for port must be converted into network byte order
	serv_addr.sin_port = htons(portno);

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		error("ERROR on binding");
	}

	/*
	 * This listen() call tells the socket to listen to the incoming connections.
	 *
	 * The listen() function places all incoming connection into a backlog queue
	 * until accept() call accepts the connection.
	 *
	 * Here, we set the maximum size for the backlog queue to 5.
	 */
	listen(sockfd, 5);

	clilen = sizeof(cli_addr);

	while (!glShutdownRequested) {

		int iResult;
		struct timeval tv;
		fd_set rfds;
		FD_ZERO(&rfds);
		FD_SET(sockfd, &rfds);

		tv.tv_sec = SELECT_TIMEOUT_SECS;
		tv.tv_usec = 0;

		iResult = select(sockfd + 1, &rfds, (fd_set *) 0, (fd_set *) 0, &tv);

		if (iResult == 0) {

			// printf("Timeout\n");

		} else if (iResult > 0) {

			int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr,
					&clilen);

			if (newsockfd < 0) {
				error("ERROR on accept");
			}

			printf("Request from %s port %d\n", inet_ntoa(cli_addr.sin_addr),
					ntohs(cli_addr.sin_port));

			syslog(LOG_INFO, "Request from %s port %d",
					inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));

			send(newsockfd, API_ID.c_str(), API_ID.length(), 0);

			close(newsockfd);

		} else if (iResult < 0) {
			error("ERROR on select");
		}
	}

	close(sockfd);
	pthread_exit(NULL);
}

/**
 *
 */
void *card_thread(void * arg) {

	SCARDCONTEXT SCardContext = 0L;
	LONG rvStatus;
	LONG rv;

	DWORD dwReadersLength;
	LPSTR szListReaders;
	SCARD_READERSTATE rgscState[MAXIMUM_SMARTCARD_READERS];
	LPTSTR szRdr;
	DWORD dwI, dwRdrCount = 0, dwPreviousRdrCount = 0;
	BOOL bFirst = TRUE;

	char szTimeBuff[64];

	execAudioOrScript("", conf.scriptStartService);

	/*
	 * Establish the Resource Manager Context, wait till available... 
	 */
	do {
		rvStatus = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL,
				&SCardContext);
		sleep(1);
	} while (rvStatus != SCARD_S_SUCCESS && !glShutdownRequested);

	/*
	 *  Look-up loop for status change.
	 */
	while (!glShutdownRequested) {

		szListReaders = NULL;
		dwRdrCount = 0;

		/*
		 * Look for readers in system.
		 */
		rvStatus = SCardListReaders(SCardContext, NULL, NULL, &dwReadersLength);

		if (rvStatus == SCARD_E_NO_READERS_AVAILABLE) {

			/*
			 * No reader is connected to the system.
			 */
			dwRdrCount = 0;

			if (glVerbose) {
				printf("Waiting for reader to connect ...\n");
			}

			// Wait 2 seconds to prevent CPU hogging.
			sleep(2);

		} else if (rvStatus != SCARD_S_SUCCESS) {

			CHECK_PCSC_RV(rvStatus, "SCardListReaders")

		} else {
			/*
			 * At least one reader is available !
			 *
			 * Allocate the size to read the list of readers and get the list
			 * of readers.
			 */
			szListReaders = (LPSTR) malloc(dwReadersLength);

			rvStatus = SCardListReaders(SCardContext, NULL,
					(LPTSTR) szListReaders, &dwReadersLength);

			CHECK_PCSC_RV(rvStatus, "SCardListReaders")

			/*
			 * Track events on found readers.
			 */
			szRdr = szListReaders;

			for (dwI = 0; dwI < MAXIMUM_SMARTCARD_READERS; dwI++) {

				if (0 == *szRdr) {
					break;
				}

				rgscState[dwI].szReader = szRdr;

				//szRdr += lstrlen(szRdr) + 1;
				szRdr += strlen(szRdr) + 1; //RRA
			}
			dwRdrCount = dwI;
		}

		/*
		 * Print the number of connected readers when the program is launched.
		 */
		if (bFirst) {
			printf("%d reader(s) connected.\n\n", (unsigned int) dwRdrCount);
			syslog(LOG_INFO, "%d reader(s) connected.",
					(unsigned int) dwRdrCount);
		}

		/*
		 * If the number of available readers has changed ...
		 */
		if (dwPreviousRdrCount != dwRdrCount) {
			/*
			 * ... set status to "unaware" for all the detected readers.
			 * An optimization could be to set this state only to the added
			 * readers and to restore the state of other available readers.
			 */
			for (dwI = 0; dwI < dwRdrCount; dwI++) {
				rgscState[dwI].dwCurrentState = SCARD_STATE_UNAWARE;
			}

			/*
			 * Print the detected event.
			 */
			if (!bFirst) {
				if (dwRdrCount < dwPreviousRdrCount) {
					printf("A reader has been removed !\n\n");
					syslog(LOG_INFO, "A reader has been removed!");
				} else {
					printf(
							"A reader has been connected during the last %d ms!\n\n",
							CARD_STATUS_CHANGE_TIMEOUT_MSEC);
					syslog(
					LOG_INFO,
							"A reader has been connected during the last %d ms!",
							CARD_STATUS_CHANGE_TIMEOUT_MSEC);
				}
			}
		}

		dwPreviousRdrCount = dwRdrCount;

		/*
		 * Call the SCardGetStatusChange to detect configuration changes on all
		 * detected readers.
		 *
		 * NOTE: You can use the INFINITE value to wait for an event on the
		 * selected reader. In that case, don't forget to use SCardCancel(...)
		 * function to end this blocking call when you want to end this process.
		 */
		if (glVerbose) {
			printf("[%s] Wait (%i msec) for status change ...\n",
					get_time(szTimeBuff, sizeof(szTimeBuff)),
					CARD_STATUS_CHANGE_TIMEOUT_MSEC);
		}

		rvStatus = SCardGetStatusChange(SCardContext,
				(DWORD) CARD_STATUS_CHANGE_TIMEOUT_MSEC, rgscState, dwRdrCount);

		if (rvStatus == SCARD_S_SUCCESS) {

			for (dwI = 0; dwI < dwRdrCount; dwI++) {

				/*
				 * Report the status for all the reader where a change has been
				 * detected.
				 *
				 * Don't forget that, in this sample, adding or removing a
				 * reader reset to unaware the status of all readers!
				 *
				 */
				DWORD eventState = rgscState[dwI].dwEventState;

				if ((SCARD_STATE_CHANGED & eventState) && !bFirst) {

					if (glVerbose) {

						printf("Reader: %s\n", rgscState[dwI].szReader);

						if (SCARD_STATE_IGNORE & eventState) {
							printf("SCARD_STATE_IGNORE: %s\n",
									"this reader should be ignored.");
						}
						if (SCARD_STATE_UNKNOWN & eventState) {
							/*
							 * If this bit is set, then SCARD_STATE_CHANGED and
							 * SCARD_STATE_IGNORE will also be set.
							 */
							printf("SCARD_STATE_UNKNOWN: %s\n",
									"reader not recognized by resource manager.");
						}
						if (SCARD_STATE_UNAVAILABLE & eventState) {
							/*
							 * If this bit is set, then all the following bits
							 * are clear.
							 */
							printf("SCARD_STATE_UNAVAILABLE: %s\n",
									"reader is not available.");
						}
						if (SCARD_STATE_EMPTY & eventState) {
							/*
							 * If this bit is set, all the following bits will
							 * be clear.
							 */
							printf("SCARD_STATE_EMPTY: %s\n",
									"There is no card in the reader.");
						}
						if (SCARD_STATE_PRESENT & eventState) {
							printf("SCARD_STATE_PRESENT: %s\n",
									"There is a card in the reader.");
						}
						if (SCARD_STATE_ATRMATCH & eventState) {
							/*
							 * If this bit is set, SCARD_STATE_PRESENT will also
							 * be set. This bit is only returned on the
							 * SCardLocateCards function.
							 */
							printf("SCARD_STATE_ATRMATCH: %s\n",
									"There is a card in the reader with an ATR matching one of the target cards.");
						}
						if (SCARD_STATE_EXCLUSIVE & eventState) {
							/*
							 * If this bit is set, SCARD_STATE_PRESENT will
							 * also be set.
							 */
							printf("SCARD_STATE_EXCLUSIVE: %s\n",
									"The card in the reader is allocated for exclusive use by another application.");
						}
						if (SCARD_STATE_INUSE & eventState) {
							/*
							 * If this bit is set, SCARD_STATE_PRESENT will
							 * also be set.
							 */
							printf("SCARD_STATE_INUSE: %s\n",
									"The card in the reader is in use by one or more other applications, but may be connected to in shared mode.");
						}
						if (SCARD_STATE_MUTE & eventState) {
							printf("SCARD_STATE_MUTE: %s\n",
									"There is an unresponsive card in the reader.");
						}
						printf("\n");
					}

					/*
					 * Check if a Card was detected.
					 *
					 * NOTE: The event state can simultaneously be
					 * 	     SCARD_STATE_PRESENT and SCARD_STATE_MUTE.
					 * 	     We ignore SCARD_STATE_MUTE.
					 */

					if ((SCARD_STATE_MUTE & rgscState[dwI].dwEventState)) {

						printf("Unresponsive card in reader [%s]\n",
								rgscState[dwI].szReader);
						syslog(LOG_INFO, "Unresponsive card in reader [%s]",
								rgscState[dwI].szReader);

					} else if ((SCARD_STATE_PRESENT
							& rgscState[dwI].dwEventState)) {

						//RRA: http://en.wikipedia.org/wiki/APDU
						//RRA: http://danm.de/docs/pcsc-sharp/PCSC/SCardReader.html

						/* connect to a card */
						SCARDHANDLE hCard;
						const SCARD_IO_REQUEST *pioSendPci;
						SCARD_IO_REQUEST pioRecvPci;
						DWORD dwActiveProtocol = -1;

						rv = SCardConnect(SCardContext, rgscState[dwI].szReader,
						SCARD_SHARE_SHARED,
						SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &hCard,
								&dwActiveProtocol);

						CHECK_PCSC_RV(rv, "SCardConnect")

						if (glVerbose) {
							printf(" Protocol: %ld\n", dwActiveProtocol);
						}

						switch (dwActiveProtocol) {
						case SCARD_PROTOCOL_T0:
							pioSendPci = SCARD_PCI_T0;
							break;
						case SCARD_PROTOCOL_T1:
							pioSendPci = SCARD_PCI_T1;
							break;
						default:
							PCSC_ERROR("Unknown protocol")
							break;
						}

						/* begin transaction */
						rv = SCardBeginTransaction(hCard);
						CHECK_PCSC_RV(rv, "SCardBeginTransaction")

						/* exchange APDU */
						BYTE pbRecvBuffer[10];
						BYTE pbSendBuffer[] = { 0xFF, // the instruction class
								0xCA, // the instruction code
								0x00, // parameter to the instruction
								0x00, // parameter to the instruction
								0x00 // size of I/O transfer
								};

						DWORD dwSendLength, dwRecvLength;

						dwSendLength = sizeof(pbSendBuffer);
						dwRecvLength = sizeof(pbRecvBuffer);

						if (glVerbose) {
							printf("Sending: ");
							int i;
							for (i = 0; i < dwSendLength; i++) {
								printf("%02X ", pbSendBuffer[i]);
							}
							printf("\n");
						}

						rv = SCardTransmit(hCard, pioSendPci, pbSendBuffer,
								dwSendLength, &pioRecvPci, pbRecvBuffer,
								&dwRecvLength);

						CHECK_PCSC_RV(rv, "SCardTransmit")

						/* end transaction */
						rv = SCardEndTransaction(hCard, SCARD_LEAVE_CARD);
						CHECK_PCSC_RV(rv, "SCardEndTransaction")

						/* card disconnect */
						rv = SCardDisconnect(hCard, SCARD_UNPOWER_CARD);
						CHECK_PCSC_RV(rv, "SCardDisconnect")

						/*
						 * NOTE: SCardEndTransaction and SCardDisconnect MUST
						 * be executed first, so this notification is last
						 * (note when the host is down a long timeout can
						 * happen before the function returns)
						 */
						DWORD dwUidLen = dwRecvLength - 2; // strip last 2 bytes

						notifyCardUid(pbRecvBuffer, dwUidLen,
								rgscState[dwI].szReader); // !!!
					}

				}

				/*
				 * Memorize the current state.
				 */
				rgscState[dwI].dwCurrentState = rgscState[dwI].dwEventState;
			}
		} else {
			if (glVerbose) {
				printf("SCardGetStatusChange returns [0x%lX]\n", rvStatus);
			}
		}

		if (szListReaders) {
			free(szListReaders);
			szListReaders = NULL;
		}

		bFirst = FALSE;

	} // end-while

//-----------------------------------------------------------------------
// GOTO LABEL (considered harmful :-)
//-----------------------------------------------------------------------
	end:

	/*
	 * Resource Manager Context: Release
	 */
	if (SCardContext) {
		rv = SCardReleaseContext(SCardContext);
		if (rv != SCARD_S_SUCCESS) {
			printf("SCardReleaseContext: %s (0x%lX)\n",
					pcsc_stringify_error(rv), rv);
		}
	}

	/*
	 * Free allocated memory.
	 */
	if (szListReaders) {
		free(szListReaders);
	}

	pthread_exit(NULL);
}

//----------------------------------------------------------------------
// MAIN entry point
//----------------------------------------------------------------------
int main(int argc, const char *argv[]) {

#ifdef PRODUCT_VERSION
	glProgramVersion = PRODUCT_VERSION;
#endif

	setlogmask(LOG_UPTO(LOG_INFO));

	openlog(argv[0], LOG_CONS | LOG_PID | LOG_NDELAY,
	LOG_LOCAL1);

	syslog(LOG_NOTICE, "Started by uid %d", getuid());

	if (!handleParms(argc, argv)) {
		return EXIT_FAILURE;
	}

	if (glVerbose) {
		setlogmask(LOG_UPTO(LOG_DEBUG));
	}

	/*
	 * The server part.
	 */
	glShutdownRequested = FALSE;
	(void) signal(SIGINT, signalHandler);
	(void) signal(SIGTERM, signalHandler);

	pthread_t serverThread;

	if (pthread_create(&serverThread, NULL, &server_thread, (void*) NULL)) {
		error("ERROR: Create of Server Thread failed");
	}

	if (glVerbose) {
		printf("Server thread started.\n");
	}

	/*
	 * The read card part.
	 */
	pthread_t cardThread;

	if (pthread_create(&cardThread, NULL, &card_thread, (void*) NULL)) {
		error("ERROR: Create of Card Thread failed");
	}

	if (glVerbose) {
		printf("Card reader thread started.\n");
	}

	/*
	 *  Welcome message
	 */
	printf("\n%s v%s : press Ctrl+C to exit\n", glProgramName,
			glProgramVersion.c_str());

	/*
	 * Block the Server thread until it terminates...
	 */
	void * pvRet;
	int tstatus;

	tstatus = pthread_join(serverThread, &pvRet);
	if (tstatus == 0) {
		//
	}

	/*
	 * Brute cancel of the Card thread.
	 */
	//pthread_cancel(cardThread);
	/*
	 * Block the Card thread until it terminates...
	 */
	tstatus = pthread_join(cardThread, &pvRet);
	if (tstatus == 0) {
		//
	}

	/*
	 *
	 */
	printf("Bye.\n");
	syslog(LOG_NOTICE, "Bye");

	closelog();

	return EXIT_SUCCESS;
}

/* end-of-file */
